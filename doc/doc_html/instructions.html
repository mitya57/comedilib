<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>4.2.  Instructions for multiple acquisitions</title><link rel="stylesheet" type="text/css" href="comedilib.css"><meta name="generator" content="DocBook XSL Stylesheets V1.79.1"><link rel="home" href="index.html" title="Comedi"><link rel="up" href="acquisitionfunctions.html" title="4.  Acquisition and configuration functions"><link rel="prev" href="acquisitionfunctions.html" title="4.  Acquisition and configuration functions"><link rel="next" href="instructionsconfiguration.html" title="4.3.  Instructions for configuration"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">4.2. 
Instructions for multiple acquisitions
</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="acquisitionfunctions.html">Prev</a> </td><th width="60%" align="center">4. 
Acquisition and configuration functions
</th><td width="20%" align="right"> <a accesskey="n" href="instructionsconfiguration.html">Next</a></td></tr></table><hr></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="instructions"></a>4.2. 
Instructions for multiple acquisitions
</h3></div></div></div><div class="toc"><dl class="toc"><dt><span class="section"><a href="instructions.html#comediinsnstructure">4.2.1. 
The instruction data structure
</a></span></dt><dt><span class="section"><a href="instructions.html#instructionexecution">4.2.2. 
Instruction execution
</a></span></dt></dl></div><p>
The <span class="emphasis"><em>instruction</em></span> is one of the most generic,
overloaden and flexible functions in the <a class="ulink" href="http://www.comedi.org" target="_top"><acronym class="acronym">Comedi</acronym></a> API. It is used to
execute a multiple of identical acquisitions on the same channel, but
also to perform a
<a class="link" href="instructionsconfiguration.html" title="4.3.  Instructions for configuration">configuration</a> of a
channel.
<a name="anchor.instruction.list"></a>
An <span class="emphasis"><em>instruction list</em></span> is a list of instructions,
possibly on different channels. Both instructions and instructions
lists are executed <span class="emphasis"><em>synchronously</em></span>, i.e., while
<span class="strong"><strong>blocking</strong></span> the calling process.
This is one of the limitations of instructions; the other one is that
they cannot code an acquisition involving timers or external events.
These limits are eliminated by the
<a class="link" href="commandsstreaming.html" title="4.5.  Commands for streaming acquisition">command</a> acquisition
primitive.
</p><div class="section"><div class="titlepage"><div><div><h4 class="title"><a name="comediinsnstructure"></a>4.2.1. 
The instruction data structure
</h4></div></div></div><p>
All the information needed to execute an instruction is stored in the
<span class="type"><a class="link" href="datatypesstructures.html#ref-type-comedi-insn" title="5.3.8.  comedi_insn">comedi_insn</a></span>
data structure:
</p><pre class="programlisting">
typedef struct <a name="insn-data-structure"></a>comedi_insn_struct {
  <a name="insn-data-structure-insn"></a>unsigned int insn;      // integer encoding the type of acquisition
                          // (or configuration)
  unsigned int n;         // number of elements in data array
  <a class="link" href="datatypesstructures.html#ref-type-lsampl-t" title="5.3.4.  lsampl_t">lsampl_t</a> <a name="insn-data-structure-data"></a>*data;         // pointer to data buffer
  unsigned int subdev;    // subdevice
  unsigned int <a name="insn-data-structure-chanspec"></a><a class="link" href="constantsmacros.html#ref-macro-CR-PACK" title="5.2.1.  CR_PACK">chanspec</a>; // encoded channel specification
  unsigned int unused[3];
} comedi_insn;
</pre><p>
Because of the large flexibility of the instruction function, many
types of instruction do not need to fill in all fields, or attach
different meanings to the same field. But the current implementation
of <a class="ulink" href="http://www.comedi.org" target="_top"><acronym class="acronym">Comedi</acronym></a> requires the
<em class="structfield"><code><a class="link" href="instructions.html#insn-data-structure-data">data</a></code></em>
field to be at least one byte long.
</p><p>
The <em class="structfield"><code><a class="link" href="instructions.html#insn-data-structure-insn">insn</a></code></em> member of the
<a class="link" href="instructions.html#insn-data-structure">instruction data structure</a>
determines the type of acquisition executed in the corresponding
instruction:
</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
<code class="constant">INSN_READ</code>: the instruction executes a read on an
analog channel.
</p></li><li class="listitem"><p>
<code class="constant">INSN_WRITE</code>: the instruction executes a write on an
analog channel.
</p></li><li class="listitem"><p>
<code class="constant">INSN_BITS</code>: indicates that the instruction must
read or write values on multiple digital I/O channels.
</p></li><li class="listitem"><p>
<code class="constant">INSN_GTOD</code>: the instruction performs a
<span class="quote">“<span class="quote">Get Time Of Day</span>”</span> acquisition.
</p></li><li class="listitem"><p>
<code class="constant">INSN_WAIT</code>: the instruction blocks for a specified
number of nanoseconds.
</p></li></ul></div><p>
</p></div><div class="section"><div class="titlepage"><div><div><h4 class="title"><a name="instructionexecution"></a>4.2.2. 
Instruction execution
</h4></div></div></div><p>
Once an instruction data structure has been filled in, the
corresponding instruction is executed with the function
<code class="function"><a class="link" href="func-ref-comedi-do-insn.html" title="comedi_do_insn">comedi_do_insn</a></code>:

</p><div class="funcsynopsis"><table border="0" class="funcprototype-table" summary="Function synopsis" style="cellspacing: 0; cellpadding: 0;"><tr><td><code class="funcdef">int <b class="fsfunc">comedi_do_insn</b>(</code></td><td>comedi_t *<var class="pdparam">device</var>, </td></tr><tr><td> </td><td>comedi_insn *<var class="pdparam">instruction</var><code>)</code>;</td></tr></table><div class="funcprototype-spacer"> </div></div><p>

Many <a class="ulink" href="http://www.comedi.org" target="_top"><acronym class="acronym">Comedi</acronym></a> instructions are shortcuts that relieve the programmer
from explicitly filling in the data structure and calling the
<code class="function"><a class="link" href="func-ref-comedi-do-insn.html" title="comedi_do_insn">comedi_do_insn</a></code>
function.
</p><p>
A list of instructions can be executed in one function call using the function
<code class="function"><a class="link" href="func-ref-comedi-do-insnlist.html" title="comedi_do_insnlist">comedi_do_insnlist</a></code>:

</p><div class="funcsynopsis"><table border="0" class="funcprototype-table" summary="Function synopsis" style="cellspacing: 0; cellpadding: 0;"><tr><td><code class="funcdef">int <b class="fsfunc">comedi_do_insnlist</b>(</code></td><td>comedi_t *<var class="pdparam">device</var>, </td></tr><tr><td> </td><td>comedi_insnlist *<var class="pdparam">list</var><code>)</code>;</td></tr></table><div class="funcprototype-spacer"> </div></div><p>

The parameter <em class="parameter"><code>list</code></em> is a pointer to a
<span class="type"><a class="link" href="instructions.html#insnlist-data-structure">comedi_insnlist</a></span>
data structure holding a pointer to an array of <span class="type">comedi_insn</span>
and the number of instructions in the list:
</p><pre class="programlisting">
typedef struct <a name="insnlist-data-structure"></a>comedi_insnlist_struct {
  unsigned int n_insns;
  comedi_insn *insns;
} comedi_insnlist;
</pre><p>
</p><p>
The number of instructions in the list is limited in the
implementation, because instructions are executed
<span class="emphasis"><em>synchronously</em></span>, i.e., the call blocks until the
whole instruction (list) has finished.
</p></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="acquisitionfunctions.html">Prev</a> </td><td width="20%" align="center"><a accesskey="u" href="acquisitionfunctions.html">Up</a></td><td width="40%" align="right"> <a accesskey="n" href="instructionsconfiguration.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">4. 
Acquisition and configuration functions
 </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> 4.3. 
Instructions for configuration
</td></tr></table></div></body></html>
